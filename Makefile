#
#  Copyright (c) 2008-2014,
#  Reto Buerki <reet@codelabs.ch>
#
#  This file is part of PCSC/Ada.
#
#  PCSC/Ada is free software; you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published
#  by the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
#  PCSC/Ada is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with PCSC/Ada; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#  MA  02110-1301  USA
#

PREFIX ?= $(HOME)/libraries

MAJOR   = 0
MINOR   = 7
REV     = 7
# Set VERSION= for a static build.
VERSION = $(MAJOR).$(MINOR).$(REV)
PCSCADA = libpcscada-$(VERSION)

# This variable explicitly exists for override by people with a
# different directory hierarchy.
GPRINSTALLFLAGS ?= \
  --prefix=$(PREFIX) \
  --no-manifest \
  --ali-subdir=lib/ada/adalib/pcscada \
  --lib-subdir=lib \
  --sources-subdir=share/ada/adainclude/pcscada \
  # EOL

TARBALL = $(PCSCADA).tar.bz2

NUM_CPUS ?= 1

# GNAT_BUILDER_FLAGS, ADAFLAGS, LDFLAGS and GNATFLAGS may be overridden in the
# environment or on the command line.
GNAT_BUILDER_FLAGS ?= -R -j$(NUM_CPUS)
GNATFLAGS          ?= ${GNAT_BUILDER_FLAGS}
# GMAKE_OPTS should not be overridden because -p is essential.
GMAKE_OPTS = -p $(GNATFLAGS) \
  $(foreach v,ADAFLAGS LDFLAGS,'-X$(v)=$($(v))')

all: build-lib

build-lib:
	gprbuild $(GMAKE_OPTS) -Ppcscada -XVERSION=$(VERSION)

build-utests:
	gprbuild $(GMAKE_OPTS) -Ppcscada_utests -XVERSION=

build-itests:
	gprbuild $(GMAKE_OPTS) -Ppcscada_itests -XVERSION=

build-examples:
	gprbuild $(GMAKE_OPTS) -Ppcscada_examples -XVERSION=

clean:
	@rm -rf lib/ obj/

# run unit tests
utests: build-utests
	@obj/utests/runner

# run 'integration' tests
# you need a reader and smartcard for this to work
itests: build-itests
	@obj/itests/test_pcscada

install: build-lib
	gprinstall -f -p $(GPRINSTALLFLAGS) -Ppcscada -XVERSION=$(VERSION)

dist:
	@echo "Creating release tarball $(PCSCADA) ... "
	@git archive --format=tar HEAD --prefix $(PCSCADA)/ | bzip2 > $(TARBALL)

include doc/doc.mk
